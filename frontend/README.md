# Frontend

This provides a SPA web app written in react.

## Requirements

- NodeJS (v14.5.0 or up)

## Development

The web app uses the `API_URL` environment variable to connect to the API.

```shell
export API_URL="https://test.api.todo.glacion.com"
yarn
yarn dev
```

## Testing

Integration tests will be run via cypress, by default cypress will run against `http://localhost:8000`, change the `cypress.json` file or export the environment variable `CYPRESS_BASE_URL` to wherever the frontend app is running.

To open the test suite, run:

```shell
  yarn run cypress open
```

This will reload tests when they change.

## Code Style

Make sure the code conforms to the given `eslint` config.

## Licence

This is based from Krunoslav Baranac's work on [GitHub](https://github.com/kunokdev/cra-runtime-environment-variables)
