import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, Container, CssBaseline } from '@material-ui/core';
import Form from './Form';
import ListItem from './ListItem';
import AppBar from './AppBar';
import { get, post, put, remove } from './api';

const useStyles = makeStyles((theme) => ({
  content: {
    paddingTop: theme.spacing(4),
  },
}));

const App = () => {
  const classes = useStyles();

  const [todo, setTodo] = useState('');
  const [todos, setTodos] = useState([]);
  const [progress, setProgress] = useState(false);

  const getTodos = () => {
    setProgress(true);
    get()
      .then((fetchedTodos) => setTodos(fetchedTodos))
      .then(() => setProgress(false))
      .catch((err) => console.log(err));
  };

  const deleteTodo = (id) => {
    setProgress(true);
    remove(id)
      .then((response) => console.log(response.status))
      .then(() => getTodos())
      .catch((err) => console.log(err));
  };

  const postTodo = (todoText) => {
    setProgress(true);
    post(todoText)
      .then((response) => console.log(response.status))
      .then(() => getTodos())
      .catch((err) => console.log(err));
  };

  const onTodoClick = (todoObject) => {
    setProgress(true);
    put(todoObject)
      .then((response) => console.log(response.status))
      .then(() => getTodos())
      .catch((err) => console.log(err));
  };

  const onTodoSubmit = (event) => {
    event.preventDefault();
    postTodo(todo);
  };

  useEffect(getTodos, []);
  return (
    <CssBaseline>
      <AppBar progress={progress} />
      <Container maxWidth='sm' className={classes.content}>
        <Form onSubmit={onTodoSubmit} onChange={setTodo} value={todo} />
        <List data-cy='todo-list'>
          {todos.map((todoObject) =>
            ListItem(todoObject, onTodoClick, deleteTodo),
          )}
        </List>
      </Container>
    </CssBaseline>
  );
};

export default App;
