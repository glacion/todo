const { API_URL } = window._env_;

const get = async () => {
  const response = await fetch(API_URL);
  const data = await response.json();
  return data;
};

const post = async (todo) => {
  const response = await fetch(`${API_URL}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ title: todo, completed: false }),
  });
  return response;
};

const put = async (todo) => {
  const response = await fetch(`${API_URL}/${todo.id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      title: todo.title,
      completed: !todo.completed,
    }),
  });
  return response;
};

const remove = async (id) => {
  const response = fetch(`${API_URL}/${id}`, {
    method: 'DELETE',
  });
  return response;
};

export { get, post, put, remove };
