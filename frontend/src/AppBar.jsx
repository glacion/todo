import { makeStyles } from '@material-ui/core/styles';
import {
  AppBar as MaterialAppBar,
  Toolbar,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));

const AppBar = ({ progress }) => {
  const classes = useStyles();

  return (
    <MaterialAppBar position='relative' data-cy='appbar'>
      <Toolbar>
        <Typography variant='h6' className={classes.title} data-cy='todos-logo'>
          Todos
        </Typography>
        {progress ? <CircularProgress color='inherit' /> : null}
      </Toolbar>
    </MaterialAppBar>
  );
};

export default AppBar;
