import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  submit: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
  },
}));

const Form = ({ onSubmit, onChange, value }) => {
  const classes = useStyles();
  return (
    <form noValidate onSubmit={(event) => onSubmit(event)} data-cy='todo-form'>
      <TextField
        name='todo'
        variant='outlined'
        required
        fullWidth
        data-cy='todo-input'
        label='What do you need to do?'
        value={value}
        onChange={(event) => onChange(event.target.value)}
      />
      <Button
        type='submit'
        fullWidth
        variant='contained'
        color='primary'
        className={classes.submit}
        data-cy='todo-submit'
      >
        <Typography variant='button'>Add Todo</Typography>
      </Button>
    </form>
  );
};

export default Form;
