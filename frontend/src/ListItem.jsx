import {
  ListItem as MaterialListItem,
  ListItemText,
  Typography,
  ListItemSecondaryAction,
  IconButton,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import React from 'react';

const TodoListItem = (todo, onClick, onDelete) => (
  <MaterialListItem
    button
    key={todo.id}
    onClick={() => onClick(todo)}
    data-cy='list-item'
  >
    <ListItemText
      disableTypography
      primary={
        <Typography
          variant='body1'
          style={
            todo.completed
              ? { textDecoration: 'line-through' }
              : { textDecoration: 'none' }
          }
        >
          {todo.title}
        </Typography>
      }
    />
    <ListItemSecondaryAction>
      <IconButton
        edge='end'
        aria-label='delete'
        data-cy='delete-icon'
        onClick={() => onDelete(todo.id)}
      >
        <DeleteIcon />
      </IconButton>
    </ListItemSecondaryAction>
  </MaterialListItem>
);

export default TodoListItem;
