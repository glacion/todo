describe('Add todo', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitUntil(() => cy.get('[data-cy=todo-form]').should('be.visible'));
    cy.waitUntil(() => cy.get('[data-cy=todo-list]').should('be.visible'));
  });

  it('Adds a todo to page.', () => {
    cy.get('[data-cy=todo-form]').within(() => {
      cy.get('[data-cy=todo-input]').type('buy some milk');
      cy.get('[data-cy=todo-submit]').click();
      cy.get('input').clear();
    });
  });

  it('Contains given text.', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li').should((li) => {
        expect(li.last(), 'latest item').to.contain('buy some milk');
      });
    });
  });
});

describe('Add another todo', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitUntil(() => cy.get('[data-cy=todo-form]').should('be.visible'));
    cy.waitUntil(() => cy.get('[data-cy=todo-list]').should('be.visible'));
  });

  it('Has latest item buy some milk', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li').should((li) => {
        expect(li.first(), 'first item').to.contain('buy some milk');
      });
    });
  });

  it('Adds a todo to page.', () => {
    cy.get('[data-cy=todo-form]').within(() => {
      cy.get('[data-cy=todo-input]').type('enjoy the assignment');
      cy.get('[data-cy=todo-submit]').click();
      cy.get('input').clear();
    });
  });

  it('Contains given text.', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li').should((li) => {
        expect(li.first(), 'first item').to.contain('enjoy the assignment');
      });
    });
  });
});

describe('Mark todo as completed', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitUntil(() => cy.get('[data-cy=todo-form]').should('be.visible'));
    cy.waitUntil(() => cy.get('[data-cy=todo-list]').should('be.visible'));
  });

  it('Clicks on todo', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .contains('buy some milk')
        .click();
    });
  });

  it('Has its text line-through', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .contains('buy some milk')
        .should('have.css', 'text-decoration')
        .and('match', /line-through/);
    });
  });
});

describe('Mark todo as uncompleted', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitUntil(() => cy.get('[data-cy=todo-form]').should('be.visible'));
    cy.waitUntil(() => cy.get('[data-cy=todo-list]').should('be.visible'));
  });

  it('Clicks on todo', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .contains('buy some milk')
        .click();
    });
  });

  it('Has its text-decoration none', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .contains('buy some milk')
        .should('have.css', 'text-decoration')
        .and('match', /none/);
    });
  });
});

describe('Add and delete todo', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitUntil(() => cy.get('[data-cy=todo-form]').should('be.visible'));
    cy.waitUntil(() => cy.get('[data-cy=todo-list]').should('be.visible'));
  });

  it('Adds todo with text "rest for a while"', () => {
    cy.get('[data-cy=todo-form]').within(() => {
      cy.get('[data-cy=todo-input]').type('rest for a while');
      cy.get('[data-cy=todo-submit]').click();
      cy.get('input').clear();
    });
  });

  it('Deletes todo with text "rest for a while"', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .last()
        .within(() => cy.get('[data-cy=delete-icon]').click());
    });
  });

  it("Ensures the todo doesn't exist", () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .last()
        .should('not.have.text', 'rest for a while');
    });
  });
});

describe('Add two todos and delete one', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitUntil(() => cy.get('[data-cy=todo-form]').should('be.visible'));
    cy.waitUntil(() => cy.get('[data-cy=todo-list]').should('be.visible'));
  });

  it('Adds todo with text "drink water"', () => {
    cy.get('[data-cy=todo-form]').within(() => {
      cy.get('[data-cy=todo-input]').type('drink water');
      cy.get('[data-cy=todo-submit]').click();
      cy.get('input').clear();
    });
  });

  it('Adds todo with text "rest for a while"', () => {
    cy.get('[data-cy=todo-form]').within(() => {
      cy.get('[data-cy=todo-input]').type('rest for a while');
      cy.get('[data-cy=todo-submit]').click();
      cy.get('input').clear();
    });
  });

  it('Deletes todo with text "rest for a while"', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .first()
        .within(() => cy.get('[data-cy=delete-icon]').click());
    });
  });

  it('Ensures the first item is "drink water"', () => {
    cy.get('[data-cy=todo-list]').within(() => {
      cy.get('li')
        .first()
        .should('have.text', 'drink water');
    });
  });
});