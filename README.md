# Todo App

A Todo app comprised of PostgreSQL/Fastapi/Python/React.

Documentation of services can be found in their respective directories.

## Development

Run `docker-compose` with `dev.compose.yml` file to start up a database, pgadmin, backend, and frontend services. Backend and frontend containers will reload on code changes. For local setup of these services, consult the readme files in their respective directories.

## Deployment

Project can be deployed on a docker host using `docker-compose` and the `prod.compose.yml` file

## CI/CD Pipeline

Gitlab CI/CD will run for each commit, steps are as follows:

- If there are changes in backend folder, run unit tests for backend.
- Run cypress specs.
- Deploy this revision of code with the proxy services.

## Site

Site is available at [https://todo.glacion.com]  
API is available at [https://api.todo.glacion.com]  
API documentation is available at [https://api.todo.glacion.com/docs] or [https://api.todo.glacion.com/redoc]
Pgadmin is available at [https://pg.toto.glacion.com]

## Testing

Cypress is used to run e2e tests, run `docker-compose` with `cypress.compose.yml` to run them.
