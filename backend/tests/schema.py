import pytest
from pydantic import ValidationError

from todo.schema.todo import Todo


def test_correct_model():
    Todo(
        id=1,
        title="Test title",
        created_at="2020-10-15T16:43:44Z",
        completed=True,
    )


def test_empty_title():
    try:
        Todo(
            id=1,
            title="",
            created_at="2020-07-15T16:43:44Z",
            completed=True,
        )
    except ValidationError as error:
        assert "ensure this value has at least 1 characters" in str(error)
    else:
        assert False


def test_long_title():
    try:
        Todo(
            id=1,
            title="Test titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest titleTest title",
            created_at="2020-07-15T16:43:44Z",
            completed=True,
        )
    except ValidationError as error:
        assert "ensure this value has at most 255 characters" in str(error)
    else:
        assert False


def test_invalid_id():
    try:
        Todo(
            id=0,
            title="Test title",
            created_at="2020-07-15T16:43:44Z",
            completed=True,
        )
    except ValidationError as error:
        assert "ensure this value is greater than 0" in str(error)
    else:
        assert False
