import pytest
from todo.database import database
from todo.repository.todo import TodoRepository
from todo.schema.todo import Todo, TodoAdd, TodoUpdate
from todo.exception import NotFoundException


@pytest.fixture()
async def db():
    await database.connect()
    yield database
    await database.disconnect()


@pytest.mark.asyncio
async def test_add_todo(db):
    repo = TodoRepository(db)
    todo = await repo.add_todo(TodoAdd(title="Test todo", completed=True))
    assert todo.title == "Test todo"
    assert todo.completed == True
    assert todo.id
    assert todo.created_at


@pytest.mark.asyncio
async def test_update_todo(db):
    repo = TodoRepository(db)
    todo = await repo.add_todo(TodoAdd(title="Test todo", completed=True))
    assert todo.title == "Test todo"
    assert todo.completed == True
    todo = await repo.update_todo(
        todo.id,
        TodoUpdate(
            title="Changed title",
            completed=False,
        ),
    )
    assert todo.title == "Changed title"
    assert todo.completed == False


@pytest.mark.asyncio
async def test_delete_todo(db):
    repo = TodoRepository(db)
    todo = await repo.add_todo(TodoAdd(title="Test todo", completed=True))
    assert todo.title == "Test todo"
    assert todo.completed == True
    await repo.delete_todo(todo.id)
    try:
        await repo.get_todo(todo.id)
    except NotFoundException:
        return True
    else:
        return False


@pytest.mark.asyncio
async def test_read_todo(db):
    repo = TodoRepository(db)
    todo = await repo.add_todo(TodoAdd(title="Test todo", completed=True))
    assert todo.title == "Test todo"
    assert todo.completed == True
    todo = await repo.get_todo(todo.id)
    assert todo.title == "Test todo"
    assert todo.completed == True


@pytest.mark.asyncio
async def test_read_todos(db):
    repo = TodoRepository(db)
    assert len(await repo.get_todos()) == 0
    todo = await repo.add_todo(TodoAdd(title="Test todo", completed=True))
    assert todo.title == "Test todo"
    assert todo.completed == True
    assert len(await repo.get_todos()) == 1