import pytest
from fastapi.testclient import TestClient
from todo.database import database
from todo.app import app

client = TestClient(app)


@pytest.fixture(autouse=True)
async def db():
    await database.connect()
    yield database
    await database.disconnect()


@pytest.fixture()
def post_todo():
    response = client.post("/", json={"title": "Test title", "completed": True})
    assert response.status_code == 200
    todo = response.json()
    assert todo["title"] == "Test title"
    assert todo["completed"] == True
    return todo


def test_empty_list():
    response = client.get("/")
    assert response.status_code == 200
    todos = response.json()
    assert todos == []


def test_create_todo(post_todo):
    assert post_todo


def test_get_first_todo(post_todo):
    response = client.get(f"/{post_todo['id']}")
    assert response.status_code == 200
    todo = response.json()
    assert todo["title"] == "Test title"
    assert todo["completed"] == True


def test_list_after_post(post_todo):
    response = client.get("/")
    assert response.status_code == 200
    todos = response.json()
    assert type(todos) == list
    assert len(todos) == 1


def test_update_todo(post_todo):
    response = client.put(
        f"/{post_todo['id']}", json={"title": "Updated title", "completed": False}
    )
    assert response.status_code == 200
    todo = response.json()
    assert todo["title"] == "Updated title"
    assert todo["completed"] == False


post_second_todo = post_todo


def test_create_another_todo(post_todo, post_second_todo):
    assert post_todo
    assert post_second_todo


def test_list_after_second_post(post_todo, post_second_todo):
    response = client.get("/")
    assert response.status_code == 200
    todos = response.json()
    assert type(todos) == list
    assert len(todos) == 2


def test_delete_first_todo(post_todo, post_second_todo):
    todos = client.get("/").json()
    response = client.delete(f"/{todos[0]['id']}")
    assert response.status_code == 200


def test_list_after_delete(post_todo, post_second_todo):
    response = client.delete(f"/{post_second_todo['id']}")
    assert response.status_code == 200
    response = client.get("/")
    assert response.status_code == 200
    todos = response.json()
    assert type(todos) == list
    assert len(todos) == 1


def test_list_after_delete_second(post_todo, post_second_todo):
    response = client.delete(f"/{post_todo['id']}")
    assert response.status_code == 200
    response = client.get("/")
    assert response.status_code == 200
    assert len(response.json()) == 1
    response = client.delete(f"/{post_second_todo['id']}")
    assert response.status_code == 200
    response = client.get("/")
    assert response.status_code == 200
    assert len(response.json()) == 0
