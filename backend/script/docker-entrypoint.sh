#!/bin/sh

set -e

# Activate virtual environment
. /opt/app/.venv/bin/activate

export PYTHONPATH=${PWD}

# Migrations
# Try 5 times with 10 seconds intervals if database is not up.
# TODO: https://12factor.net/admin-processes
n=0
until [ "$n" -ge 5 ]
do
   alembic upgrade head && break  # substitute your command here
   n=$((n+1)) 
   sleep 10
done

# Evaluate passed command:
exec "$@"