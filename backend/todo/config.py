from pydantic import BaseSettings
from pydantic.fields import Field


class Config(BaseSettings):
    postgres_dsn: str = Field(
        "postgresql://postgres:admin@localhost:5432/todo",
        title="PostgreSQL DSN",
        description="PostgreSQL Data Source Name",
    )
    testing: bool = Field(
        False,
        title="Whether the app is in test mode",
        description="Whether the app is in test mode",
    )


config = Config()
