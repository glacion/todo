import databases
from databases.core import Database
from sqlalchemy.sql.schema import MetaData
from todo.config import config

database: Database = databases.Database(
    config.postgres_dsn,
    force_rollback=config.testing,
)

metadata: MetaData = MetaData()
