class AddFailureException(Exception):
    pass


class DeleteFailureException(Exception):
    pass


class NotFoundException(Exception):
    pass