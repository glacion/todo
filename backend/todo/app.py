from fastapi import FastAPI, Path, Body, Depends, HTTPException
from fastapi.middleware.cors import CORSMiddleware

from todo.database import database
from todo.schema.todo import Todo, TodoAdd, TodoUpdate
from todo.repository.todo import TodoRepository
from typing import List
from todo.exception import (
    AddFailureException,
    DeleteFailureException,
    NotFoundException,
)

todo_repository = TodoRepository(database)

app = FastAPI(
    title="Todo App",
    description="Todo App",
    version="1.0.0",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/", response_model=List[Todo])
async def get_todos():
    return await todo_repository.get_todos()


@app.get("/{id}", response_model=Todo)
async def get_todo(
    id: int = Path(..., title="ID", description="ID of the todo"),
):
    try:
        return await todo_repository.get_todo(id)
    except NotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))


@app.post("/", response_model=Todo)
async def add_todo(
    todo: TodoAdd = Body(..., title="Todo", description="Todo object"),
):
    try:
        return await todo_repository.add_todo(todo)
    except AddFailureException as e:
        raise HTTPException(status_code=400, detail=str(e))


@app.put("/{id}", response_model=Todo)
async def update_todo(
    id: int = Path(..., title="ID", description="ID of the todo"),
    todo: TodoUpdate = Body(..., title="Todo", description="Todo object"),
):
    try:
        return await todo_repository.update_todo(id, todo)
    except NotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))


@app.delete("/{id}")
async def delete_todo(
    id: int = Path(..., title="ID", description="ID of the todo"),
):
    try:
        await todo_repository.delete_todo(id)
    except NotFoundException as e:
        raise HTTPException(status_code=404, detail=str(e))


if __name__ == "__main__":
    import uvicorn

    uvicorn.run("todo.app:app", host="0.0.0.0", reload=True, port=8000)
