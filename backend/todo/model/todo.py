from sqlalchemy.sql import func
from sqlalchemy.sql.schema import Column, Table
from sqlalchemy.sql.sqltypes import Boolean, DateTime, Integer, String

from todo.database import metadata

todo = Table(
    "todo",
    metadata,
    Column(
        "id",
        Integer,
        primary_key=True,
        index=True,
        unique=True,
        nullable=False,
    ),
    Column(
        "title",
        String(length=255),
        nullable=False,
    ),
    Column(
        "created_at",
        DateTime(),
        server_default=func.now(),
        nullable=False,
    ),
    Column(
        "completed",
        Boolean(),
        default=False,
    ),
)
