from typing import List

from databases import Database
from sqlalchemy.sql.expression import desc

from todo.exception import (
    AddFailureException,
    DeleteFailureException,
    NotFoundException,
)
from todo.model.todo import todo
from todo.schema.todo import Todo, TodoAdd, TodoUpdate


class TodoRepository:
    database: Database

    def __init__(self, database: Database) -> None:
        self.database = database

    async def get_todo(self, id: int) -> Todo:
        query = todo.select().where(todo.c.id == id)
        response = await self.database.fetch_one(query)
        if not response:
            raise NotFoundException("todo does not exist")
        return Todo(**response)

    async def get_todos(self) -> List[Todo]:
        query = todo.select().order_by(desc(todo.c.created_at))
        todos = await self.database.fetch_all(query)
        return [Todo(**item) for item in todos]

    async def delete_todo(self, id: int) -> None:
        obj = await self.get_todo(id)
        query = todo.delete().where(todo.c.id == id)
        await self.database.execute(query)
        try:
            await self.get_todo(id)
        except NotFoundException:
            return
        else:
            raise DeleteFailureException("failed to delete todo")

    async def add_todo(self, data: TodoAdd) -> Todo:
        dictionary = data.dict()
        query = todo.insert().values(**dictionary)
        id = await self.database.execute(query)
        try:
            added_todo = await self.get_todo(id)
            return Todo(**added_todo.dict())
        except NotFoundException:
            raise AddFailureException("failed to add todo")

    async def update_todo(self, id: int, data: TodoUpdate) -> Todo:
        obj = await self.get_todo(id)
        dictionary = data.dict(
            exclude={"id"},
            exclude_unset=True,
            exclude_defaults=True,
            exclude_none=True,
        )
        query = todo.update().where(todo.c.id == id).values(**dictionary)
        await self.database.execute(query)
        updated_todo = await self.get_todo(id)
        return Todo(**updated_todo.dict())
