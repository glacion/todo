from pydantic import BaseModel, Field
from typing import Optional
from datetime import datetime


class TodoAdd(BaseModel):
    title: str = Field(
        ...,
        title="Title",
        description="Title of the todo",
        min_length=1,
    )

    completed: bool = Field(
        False,
        title="Completed",
        description="Whether the todo is completed",
    )


class TodoUpdate(BaseModel):
    title: Optional[str] = Field(
        None,
        title="Title",
        description="Title of the todo",
        min_length=1,
    )
    completed: Optional[bool] = Field(
        None,
        title="Completed",
        description="Whether the todo is completed",
    )


class Todo(BaseModel):
    id: int = Field(
        ...,
        title="ID",
        description="ID of the todo",
        gt=0,
    )
    title: str = Field(
        ...,
        title="Title",
        description="Title of the todo",
        min_length=1,
        max_length=255,
    )
    completed: bool = Field(
        ...,
        title="Completed",
        description="Whether the todo is completed",
    )
    created_at: datetime = Field(
        ...,
        title="Creation time",
        description="Time in which this todo is created",
    )
