# Backend

This provides a REST API, written with python and fastapi.

## Requirements

- Python (3.8.3 or up)
- Poetry (1.0.9 or up)

### Running The Server

- Install the dependencies

  ```shell
  poetry install
  ```

- Activate the virtual environment

  ```shell
  poetry shell
  ```

- Start the server
  ```shell
  uvicorn todo.app:app --reload
  ```
  This provides live reloading on code changes.

### Testing

Assuming that you have followed until the start the server step, run the tests with the following

```shell
pytest -x -s
```

### Code Style

Format the code using the `black` tool

```shell
black .
```
